
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module helps us to validate the image based on the aspect ratio 
configured againest the image field.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. we would need to
configure the aspect ratio againest the image field settings.

MAINTAINERS
-----------------

Current maintainers:
 * Karteek Reddy Talusani - https://www.drupal.org/u/karteektalusanipennywisesolutions
 
This project has been sponsored by:
 * PennyWise Solutions Private Limited
   Specialized in consulting and planning of Drupal powered sites,
   PennyWise Solutions Private Limited offers installation, development,
   theming,customization, and hosting to get you started.
   Visit https://www.pennywisesolutions.com for more information.
